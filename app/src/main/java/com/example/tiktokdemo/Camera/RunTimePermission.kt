package com.example.tiktokdemo.Camera

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.text.Html
import android.util.Log
import androidx.core.content.ContextCompat
import com.example.tiktokdemo.R
import java.util.*

class RunTimePermission(private val activity: Activity) : Activity() {
    private var arrayListPermission: ArrayList<PermissionBean>? = null
    private var arrayPermissions: Array<String?>? = null
    private var runTimePermissionListener: RunTimePermissionListener? = null
    fun requestPermission(
        permissions: Array<String?>,
        runTimePermissionListener: RunTimePermissionListener?
    ) {
        this.runTimePermissionListener = runTimePermissionListener
        arrayListPermission = ArrayList()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            for (i in permissions.indices) {
                val permissionBean = PermissionBean()
                if (ContextCompat.checkSelfPermission(
                        activity,
                        permissions[i]!!
                    ) == PackageManager.PERMISSION_GRANTED
                ) {
                    permissionBean.isAccept = true
                } else {
                    permissionBean.isAccept = false
                    permissionBean.permission = permissions[i]
                    arrayListPermission!!.add(permissionBean)
                }
            }
            if (arrayListPermission!!.size <= 0) {
                runTimePermissionListener!!.permissionGranted()
                return
            }
            arrayPermissions = arrayOfNulls(arrayListPermission!!.size)
            for (i in arrayListPermission!!.indices) {
                arrayPermissions!![i] = arrayListPermission!![i].permission
            }
            activity.requestPermissions(arrayPermissions!!, 10)
        } else {
            runTimePermissionListener?.permissionGranted()
        }
    }

    private fun callSettingActivity() {
        val intent = Intent()
        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        val uri =
            Uri.fromParts("package", activity.packageName, null)
        intent.data = uri
        activity.startActivity(intent)
    }

    private fun checkUpdate() {
        var isGranted = true
        var deniedCount = 0
        for (i in arrayListPermission!!.indices) {
            if (!arrayListPermission!![i].isAccept) {
                isGranted = false
                deniedCount++
            }
        }
        if (isGranted) {
            if (runTimePermissionListener != null) {
                runTimePermissionListener!!.permissionGranted()
            }
        } else {
            if (runTimePermissionListener != null) {
//                if (deniedCount == arrayListPermission.size())
//                {
                setAlertMessage()

//                }
                runTimePermissionListener!!.permissionDenied()
            }
        }
    }

    fun setAlertMessage() {
        val adb: AlertDialog.Builder
        adb = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AlertDialog.Builder(
                activity,
                android.R.style.Theme_Material_Light_Dialog_Alert
            )
        } else {
            AlertDialog.Builder(activity)
        }
        adb.setTitle(activity.resources.getString(R.string.app_name))
        val msg = "<p>Dear User, </p>" +
                "<p>Seems like you have <b>\"Denied\"</b> the minimum requirement permission to access more features of application.</p>" +
                "<p>You must have to <b>\"Allow\"</b> all permission. We will not share your data with anyone else.</p>" +
                "<p>Do you want to enable all requirement permission ?</p>" +
                "<p>Go To : Settings >> App > " + activity.resources
            .getString(R.string.app_name) + " Permission : Allow ALL</p>"
        adb.setMessage(Html.fromHtml(msg))
        adb.setPositiveButton("Allow All") { dialog, which ->
            callSettingActivity()
            dialog.dismiss()
        }
        adb.setNegativeButton("Remind Me Later") { dialog, which -> dialog.dismiss() }
        if (!activity.isFinishing && msg.length > 0) {
            adb.show()
        } else {
            Log.v("log_tag", "either activity finish or message length is 0")
        }
    }

    private fun updatePermissionResult(permissions: String, grantResults: Int) {
        for (i in arrayListPermission!!.indices) {
            if (arrayListPermission!![i].permission == permissions) {
                arrayListPermission!![i].isAccept = grantResults == 0
                break
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        for (i in permissions.indices) {
            updatePermissionResult(permissions[i], grantResults[i])
        }
        checkUpdate()
    }

    interface RunTimePermissionListener {
        fun permissionGranted()
        fun permissionDenied()
    }

    inner class PermissionBean {
        var permission: String? = null
        var isAccept = false
    }

}