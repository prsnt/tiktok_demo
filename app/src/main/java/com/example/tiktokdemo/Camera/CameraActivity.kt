package com.example.tiktokdemo.Camera

import android.Manifest
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.hardware.Camera
import android.hardware.Camera.CameraInfo
import android.hardware.Camera.PictureCallback
import android.hardware.SensorManager
import android.media.CamcorderProfile
import android.media.MediaRecorder
import android.media.ThumbnailUtils
import android.os.*
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.view.View.OnLongClickListener
import android.view.View.OnTouchListener
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import com.example.tiktokdemo.R
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream

class CameraActivity : AppCompatActivity(), SurfaceHolder.Callback,
    View.OnClickListener {


     var surfaceHolder: SurfaceHolder? = null
     var camera: Camera? = null
     val customHandler: Handler? = Handler()
    var flag = 0
     var tempFile: File? = null
     var jpegCallback: PictureCallback? = null
    var MAX_VIDEO_SIZE_UPLOAD = 25 //MB


    override fun onResume() {
        super.onResume()
        try {
            if (myOrientationEventListener != null) myOrientationEventListener!!.enable()
        } catch (e1: Exception) {
            e1.printStackTrace()
        }
    }

     var folder: File? = null


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (runTimePermission != null) {
            runTimePermission!!.onRequestPermissionsResult(
                requestCode, permissions as Array<String>,
                grantResults!!
            )
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera)
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        runTimePermission = RunTimePermission(this)
        runTimePermission!!.requestPermission(arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ), object : RunTimePermission.RunTimePermissionListener {
            override fun permissionGranted() {
                // First we need to check availability of play services
                initControls()
                identifyOrientationEvents()

                //create a folder to get image
                folder = File(
                    Environment.getExternalStorageDirectory()
                        .toString() + "/whatsappCamera"
                )
                if (!folder!!.exists()) {
                    folder!!.mkdirs()
                }
                //capture image on callback
                captureImageCallback()
                //
                if (camera != null) {
                    val info = CameraInfo()
                    if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
                        imgFlashOnOff!!.visibility = View.GONE
                    }
                }
            }

            override fun permissionDenied() {}
        })
    }

     fun cancelSavePicTaskIfNeed() {
        if (savePicTask != null && savePicTask!!.status == AsyncTask.Status.RUNNING) {
            savePicTask!!.cancel(true)
        }
    }

     fun cancelSaveVideoTaskIfNeed() {
        if (saveVideoTask != null && saveVideoTask!!.status == AsyncTask.Status.RUNNING) {
            saveVideoTask!!.cancel(true)
        }
    }

     var savePicTask: SavePicTask? = null

    inner class SavePicTask( val data: ByteArray, rotation: Int) :
        AsyncTask<Void?, Void?, String?>() {
         var rotation = 0
        override fun onPreExecute() {}




        init {
            this.rotation = rotation
        }

        override fun doInBackground(vararg params: Void?): String? {
            TODO("Not yet implemented")
        }
    }

    @Throws(IOException::class)
    fun saveToSDCard(data: ByteArray, rotation: Int): String? {
        var imagePath: String? = ""
        try {
            val options = BitmapFactory.Options()
            options.inJustDecodeBounds = true
            BitmapFactory.decodeByteArray(data, 0, data.size, options)
            val metrics = DisplayMetrics()
            windowManager.defaultDisplay.getMetrics(metrics)
            val reqHeight = metrics.heightPixels
            val reqWidth = metrics.widthPixels
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight)
            options.inJustDecodeBounds = false
            val bitmap = BitmapFactory.decodeByteArray(data, 0, data.size, options)
            if (rotation != 0) {
                val mat = Matrix()
                mat.postRotate(rotation.toFloat())
                val bitmap1 = Bitmap.createBitmap(
                    bitmap!!,
                    0,
                    0,
                    bitmap.width,
                    bitmap.height,
                    mat,
                    true
                )
                if (bitmap != bitmap1) {
                    bitmap.recycle()
                }
                imagePath = getSavePhotoLocal(bitmap1)
                bitmap1?.recycle()
            } else {
                imagePath = getSavePhotoLocal(bitmap)
                bitmap?.recycle()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return imagePath
    }

    fun calculateInSampleSize(
        options: BitmapFactory.Options,
        reqWidth: Int,
        reqHeight: Int
    ): Int {
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1
        if (height > reqHeight || width > reqWidth) {
            inSampleSize = if (width > height) {
                Math.round(height.toFloat() / reqHeight.toFloat())
            } else {
                Math.round(width.toFloat() / reqWidth.toFloat())
            }
        }
        return inSampleSize
    }

     fun getSavePhotoLocal(bitmap: Bitmap?): String? {
        var path = ""
        try {
            val output: OutputStream
            val file = File(
                folder!!.absolutePath,
                "wc" + System.currentTimeMillis() + ".jpg"
            )
            try {
                output = FileOutputStream(file)
                bitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, output)
                output.flush()
                output.close()
                path = file.absolutePath
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return path
    }

     fun captureImageCallback() {
        surfaceHolder = imgSurface!!.holder
        surfaceHolder!!.addCallback(this)
        surfaceHolder!!.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS)
        jpegCallback = PictureCallback { data, camera ->
            refreshCamera()
            cancelSavePicTaskIfNeed()
            savePicTask = SavePicTask(data, getPhotoRotation())
            savePicTask!!.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR)
        }
    }

    inner class SaveVideoTask : AsyncTask<Void?, Void?, Void?>() {
        var thumbFilename: File? = null
        var progressDialog: ProgressDialog? = null
        override fun onPreExecute() {
            progressDialog = ProgressDialog(this@CameraActivity)
            progressDialog!!.setMessage("Processing a video...")
            progressDialog!!.show()
            super.onPreExecute()
            imgCapture?.setOnTouchListener(null)
            textCounter?.setVisibility(View.GONE)
            imgSwipeCamera?.setVisibility(View.VISIBLE)
            imgFlashOnOff?.setVisibility(View.VISIBLE)
        }



        override fun onPostExecute(aVoid: Void?) {
            super.onPostExecute(aVoid)
            if (progressDialog != null) {
                if (progressDialog!!.isShowing) {
                    progressDialog!!.dismiss()
                }
            }
            if (tempFile != null && thumbFilename != null) onVideoSendDialog(
                tempFile?.getAbsolutePath(),
                thumbFilename!!.absolutePath
            )
        }

        override fun doInBackground(vararg params: Void?): Void? {
            try {
                try {
                    myOrientationEventListener!!.enable()
                    customHandler!!.removeCallbacksAndMessages(null)
                    mediaRecorder!!.stop()
                    releaseMediaRecorder()
                    tempFile = File(folder?.getAbsolutePath() + "/" + mediaFileName + ".mp4")
                    thumbFilename =
                        File(folder?.getAbsolutePath(), "t_$mediaFileName.jpeg")
                    generateVideoThmb(tempFile?.getPath(), thumbFilename)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return null
        }
    }

     var mPhotoAngle = 90

     fun identifyOrientationEvents() {
        myOrientationEventListener =
            object : OrientationEventListener(this, SensorManager.SENSOR_DELAY_NORMAL) {
                override fun onOrientationChanged(iAngle: Int) {
                    val iLookup = intArrayOf(
                        0,
                        0,
                        0,
                        90,
                        90,
                        90,
                        90,
                        90,
                        90,
                        180,
                        180,
                        180,
                        180,
                        180,
                        180,
                        270,
                        270,
                        270,
                        270,
                        270,
                        270,
                        0,
                        0,
                        0
                    ) // 15-degree increments
                    if (iAngle != ORIENTATION_UNKNOWN) {
                        val iNewOrientation = iLookup[iAngle / 15]
                        if (iOrientation != iNewOrientation) {
                            iOrientation = iNewOrientation
                            if (iOrientation == 0) {
                                mOrientation = 90
                            } else if (iOrientation == 270) {
                                mOrientation = 0
                            } else if (iOrientation == 90) {
                                mOrientation = 180
                            }
                        }
                        mPhotoAngle = normalize(iAngle)
                    }
                }
            }
        if (myOrientationEventListener?.canDetectOrientation()!!) {
            myOrientationEventListener?.enable()
        }
    }

    var mediaRecorder: MediaRecorder? = null
    var imgSurface: SurfaceView? = null
    var imgCapture: ImageView? = null
    var imgFlashOnOff: ImageView? = null
    var imgSwipeCamera: ImageView? = null
    var runTimePermission: RunTimePermission? = null
    var textCounter: TextView? = null
    var hintTextView: TextView? = null

     fun initControls() {
        mediaRecorder = MediaRecorder()
        imgSurface = findViewById(R.id.imgSurface) as SurfaceView
        textCounter = findViewById(R.id.textCounter) as TextView
        imgCapture = findViewById(R.id.imgCapture) as ImageView
        imgFlashOnOff = findViewById(R.id.imgFlashOnOff) as ImageView
        imgSwipeCamera = findViewById(R.id.imgChangeCamera) as ImageView
        textCounter!!.visibility = View.GONE
        hintTextView = findViewById(R.id.hintTextView) as TextView
        imgSwipeCamera!!.setOnClickListener(this)
        activeCameraCapture()
        imgFlashOnOff!!.setOnClickListener(this)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        cancelSavePicTaskIfNeed()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.imgFlashOnOff -> flashToggle()
            R.id.imgChangeCamera -> {
                camera!!.stopPreview()
                camera!!.release()
                if (flag == 0) {
                    imgFlashOnOff!!.visibility = View.GONE
                    flag = 1
                } else {
                    imgFlashOnOff!!.visibility = View.VISIBLE
                    flag = 0
                }
                surfaceCreated(surfaceHolder)
            }
            else -> {
            }
        }
    }

     fun flashToggle() {
        if (flashType == 1) {
            flashType = 2
        } else if (flashType == 2) {
            flashType = 3
        } else if (flashType == 3) {
            flashType = 1
        }
        refreshCamera()
    }

     fun captureImage() {
        camera!!.takePicture(null, null, jpegCallback)
        inActiveCameraCapture()
    }

     fun releaseMediaRecorder() {
        if (mediaRecorder != null) {
            mediaRecorder!!.reset() // clear recorder configuration
            mediaRecorder!!.release() // release the recorder object
            mediaRecorder = MediaRecorder()
        }
    }


    fun refreshCamera() {
        if (surfaceHolder!!.surface == null) {
            return
        }
        try {
            camera!!.stopPreview()
            val param = camera!!.parameters
            if (flag == 0) {
                if (flashType == 1) {
                    param.flashMode = Camera.Parameters.FLASH_MODE_AUTO
                    imgFlashOnOff!!.setImageResource(R.drawable.ic_flash_auto)
                } else if (flashType == 2) {
                    param.flashMode = Camera.Parameters.FLASH_MODE_ON
                    var params: Camera.Parameters? = null
                    if (camera != null) {
                        params = camera!!.parameters
                        if (params != null) {
                            val supportedFlashModes =
                                params.supportedFlashModes
                            if (supportedFlashModes != null) {
                                if (supportedFlashModes.contains(Camera.Parameters.FLASH_MODE_TORCH)) {
                                    param.flashMode = Camera.Parameters.FLASH_MODE_TORCH
                                } else if (supportedFlashModes.contains(Camera.Parameters.FLASH_MODE_ON)) {
                                    param.flashMode = Camera.Parameters.FLASH_MODE_ON
                                }
                            }
                        }
                    }
                    imgFlashOnOff!!.setImageResource(R.drawable.ic_flash_on)
                } else if (flashType == 3) {
                    param.flashMode = Camera.Parameters.FLASH_MODE_OFF
                    imgFlashOnOff!!.setImageResource(R.drawable.ic_flash_off)
                }
            }
            refrechCameraPriview(param)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

     fun refrechCameraPriview(param: Camera.Parameters) {
        try {
            camera!!.parameters = param
            setCameraDisplayOrientation(0)
            camera!!.setPreviewDisplay(surfaceHolder)
            camera!!.startPreview()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun setCameraDisplayOrientation(cameraId: Int) {
        val info = CameraInfo()
        Camera.getCameraInfo(cameraId, info)
        var rotation = windowManager.defaultDisplay.rotation
        if (Build.MODEL.equals("Nexus 6", ignoreCase = true) && flag == 1) {
            rotation = Surface.ROTATION_180
        }
        var degrees = 0
        when (rotation) {
            Surface.ROTATION_0 -> degrees = 0
            Surface.ROTATION_90 -> degrees = 90
            Surface.ROTATION_180 -> degrees = 180
            Surface.ROTATION_270 -> degrees = 270
        }
        var result: Int
        if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360
            result = (360 - result) % 360 // compensate the mirror
        } else {
            result = (info.orientation - degrees + 360) % 360
        }
        camera!!.setDisplayOrientation(result)
    }

    //------------------SURFACE CREATED FIRST TIME--------------------//

    //------------------SURFACE CREATED FIRST TIME--------------------//
    var flashType = 1

    override fun surfaceCreated(arg0: SurfaceHolder?) {
        camera = try {
            if (flag == 0) {
                Camera.open(0)
            } else {
                Camera.open(1)
            }
        } catch (e: RuntimeException) {
            e.printStackTrace()
            return
        }
        try {
            val param: Camera.Parameters
            param = camera?.getParameters()!!
            val sizes =
                param.supportedPreviewSizes
            //get diff to get perfact preview sizes
            val displaymetrics = DisplayMetrics()
            windowManager.defaultDisplay.getMetrics(displaymetrics)
            val height = displaymetrics.heightPixels
            val width = displaymetrics.widthPixels
            val diff = (height * 1000 / width).toLong()
            var cdistance = Int.MAX_VALUE.toLong()
            var idx = 0
            for (i in sizes.indices) {
                val value =
                    (sizes[i].width * 1000).toLong() / sizes[i].height
                if (value > diff && value < cdistance) {
                    idx = i
                    cdistance = value
                }
                Log.e(
                    "WHHATSAPP",
                    "width=" + sizes[i].width + " height=" + sizes[i].height
                )
            }
            Log.e("WHHATSAPP", "INDEX:  $idx")
            val cs = sizes[idx]
            param.setPreviewSize(cs.width, cs.height)
            param.setPictureSize(cs.width, cs.height)
            camera?.setParameters(param)
            setCameraDisplayOrientation(0)
            camera?.setPreviewDisplay(surfaceHolder)
            camera?.startPreview()
            if (flashType == 1) {
                param.flashMode = Camera.Parameters.FLASH_MODE_AUTO
                imgFlashOnOff!!.setImageResource(R.drawable.ic_flash_auto)
            } else if (flashType == 2) {
                param.flashMode = Camera.Parameters.FLASH_MODE_ON
                var params: Camera.Parameters? = null
                if (camera != null) {
                    params = camera!!.parameters
                    if (params != null) {
                        val supportedFlashModes =
                            params.supportedFlashModes
                        if (supportedFlashModes != null) {
                            if (supportedFlashModes.contains(Camera.Parameters.FLASH_MODE_TORCH)) {
                                param.flashMode = Camera.Parameters.FLASH_MODE_TORCH
                            } else if (supportedFlashModes.contains(Camera.Parameters.FLASH_MODE_ON)) {
                                param.flashMode = Camera.Parameters.FLASH_MODE_ON
                            }
                        }
                    }
                }
                imgFlashOnOff!!.setImageResource(R.drawable.ic_flash_on)
            } else if (flashType == 3) {
                param.flashMode = Camera.Parameters.FLASH_MODE_OFF
                imgFlashOnOff!!.setImageResource(R.drawable.ic_flash_off)
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return
        }
    }

    override fun surfaceDestroyed(arg0: SurfaceHolder?) {
        try {
            camera!!.stopPreview()
            camera!!.release()
            camera = null
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun surfaceChanged(surfaceHolder: SurfaceHolder?, i: Int, i1: Int, i2: Int) {
        refreshCamera()
    }

    //------------------SURFACE OVERRIDE METHIDS END--------------------//

    //------------------SURFACE OVERRIDE METHIDS END--------------------//
     var timeInMilliseconds = 0L
      var startTime:kotlin.Long = android.os.SystemClock.uptimeMillis()
      var updatedTime:kotlin.Long = 0L
      var timeSwapBuff:kotlin.Long = 0L
     val updateTimerThread: Runnable = object : Runnable {
        override fun run() {
            timeInMilliseconds = SystemClock.uptimeMillis() - startTime
            updatedTime = timeSwapBuff + timeInMilliseconds
            var secs = (updatedTime / 1000) as Int
            val mins = secs / 60
            val hrs = mins / 60
            secs = secs % 60
            textCounter!!.text = String.format(
                "%02d",
                mins
            ) + ":" + String.format("%02d", secs)
            customHandler!!.postDelayed(this, 0)
        }
    }

     fun scaleUpAnimation() {
        val scaleDownX = ObjectAnimator.ofFloat(imgCapture, "scaleX", 2f)
        val scaleDownY = ObjectAnimator.ofFloat(imgCapture, "scaleY", 2f)
        scaleDownX.duration = 100
        scaleDownY.duration = 100
        val scaleDown = AnimatorSet()
        scaleDown.play(scaleDownX).with(scaleDownY)
        scaleDownX.addUpdateListener {
            val p = imgCapture!!.parent as View
            p.invalidate()
        }
        scaleDown.start()
    }

     fun scaleDownAnimation() {
        val scaleDownX = ObjectAnimator.ofFloat(imgCapture, "scaleX", 1f)
        val scaleDownY = ObjectAnimator.ofFloat(imgCapture, "scaleY", 1f)
        scaleDownX.duration = 100
        scaleDownY.duration = 100
        val scaleDown = AnimatorSet()
        scaleDown.play(scaleDownX).with(scaleDownY)
        scaleDownX.addUpdateListener {
            val p = imgCapture!!.parent as View
            p.invalidate()
        }
        scaleDown.start()
    }

    override fun onPause() {
        super.onPause()
        try {
            customHandler?.removeCallbacksAndMessages(null)
            releaseMediaRecorder() // if you are using MediaRecorder, release it first
            if (myOrientationEventListener != null) myOrientationEventListener!!.enable()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

     var saveVideoTask: SaveVideoTask? = null

     fun activeCameraCapture() {
        if (imgCapture != null) {
            imgCapture!!.alpha = 1.0f
            imgCapture!!.setOnLongClickListener(OnLongClickListener {
                hintTextView!!.visibility = View.INVISIBLE
                try {
                    if (prepareMediaRecorder()) {
                        myOrientationEventListener!!.disable()
                        mediaRecorder!!.start()
                        startTime = SystemClock.uptimeMillis()
                        customHandler!!.postDelayed(updateTimerThread, 0)
                    } else {
                        return@OnLongClickListener false
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                textCounter!!.visibility = View.VISIBLE
                imgSwipeCamera!!.visibility = View.GONE
                imgFlashOnOff!!.visibility = View.GONE
                scaleUpAnimation()
                imgCapture!!.setOnTouchListener(OnTouchListener { _, event ->
                    if (event.action == MotionEvent.ACTION_BUTTON_PRESS) {
                        return@OnTouchListener true
                    }
                    if (event.action == MotionEvent.ACTION_UP) {
                        scaleDownAnimation()
                        hintTextView!!.visibility = View.VISIBLE
                        cancelSaveVideoTaskIfNeed()
                        saveVideoTask = SaveVideoTask()
                        saveVideoTask!!.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR)
                        return@OnTouchListener true
                    }
                    true
                })
                true
            })
            imgCapture!!.setOnClickListener {
                if (isSpaceAvailable()) {
                    captureImage()
                } else {
                    Toast.makeText(
                        this@CameraActivity,
                        "Memory is not available",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    fun onVideoSendDialog(videopath: String?, thumbPath: String) {
        runOnUiThread {
            if (videopath != null) {
                val fileVideo = File(videopath)
                val fileSizeInBytes = fileVideo.length()
                val fileSizeInKB = fileSizeInBytes / 1024
                val fileSizeInMB = fileSizeInKB / 1024
                if (fileSizeInMB > MAX_VIDEO_SIZE_UPLOAD) {
                    AlertDialog.Builder(this@CameraActivity)
                        .setMessage(
                            getString(
                                R.string.file_limit_size_upload_format, MAX_VIDEO_SIZE_UPLOAD
                            )
                        )
                        .setPositiveButton("OK",
                            DialogInterface.OnClickListener { dialog, which -> dialog.dismiss() })
                        .show()
                } else {
                    val mIntent = Intent(
                        this@CameraActivity,
                        PhotoVideoRedirectActivity::class.java
                    )
                    mIntent.putExtra("PATH", videopath.toString())
                    mIntent.putExtra("THUMB", thumbPath)
                    mIntent.putExtra("WHO", "Video")
                    startActivity(mIntent)

                    //SendVideoDialog sendVideoDialog = SendVideoDialog.newInstance(videopath, thumbPath, name, phoneNuber);
                    // sendVideoDialog.show(getSupportFragmentManager(), "SendVideoDialog");
                }
            }
        }
    }

     fun inActiveCameraCapture() {
        if (imgCapture != null) {
            imgCapture!!.alpha = 0.5f
            imgCapture!!.setOnClickListener(null)
        }
    }

    //--------------------------CHECK FOR MEMORY -----------------------------//

    //--------------------------CHECK FOR MEMORY -----------------------------//
    fun getFreeSpacePercantage(): Int {
        val percantage = (freeMemory() * 100 / totalMemory()).toInt()
        val modValue = percantage % 5
        return percantage - modValue
    }

    fun totalMemory(): Double {
        val stat = StatFs(Environment.getExternalStorageDirectory().path)
        val sdAvailSize =
            stat.blockCount.toDouble() * stat.blockSize.toDouble()
        return sdAvailSize / 1073741824
    }

    fun freeMemory(): Double {
        val stat = StatFs(Environment.getExternalStorageDirectory().path)
        val sdAvailSize =
            stat.availableBlocks.toDouble() * stat.blockSize.toDouble()
        return sdAvailSize / 1073741824
    }

    fun isSpaceAvailable(): Boolean {
        return if (getFreeSpacePercantage() >= 1) {
            true
        } else {
            false
        }
    }
    //-------------------END METHODS OF CHECK MEMORY--------------------------//


    //-------------------END METHODS OF CHECK MEMORY--------------------------//
     var mediaFileName: String? = null

    @SuppressLint("SimpleDateFormat")
    @Throws(IOException::class)
     fun prepareMediaRecorder(): Boolean {
        mediaRecorder = MediaRecorder() // Works well
        camera!!.stopPreview()
        camera!!.unlock()
        mediaRecorder!!.setCamera(camera)
        mediaRecorder!!.setVideoSource(MediaRecorder.VideoSource.CAMERA)
        mediaRecorder!!.setAudioSource(MediaRecorder.AudioSource.MIC)
        if (flag == 1) {
            mediaRecorder!!.setProfile(CamcorderProfile.get(1, CamcorderProfile.QUALITY_HIGH))
        } else {
            mediaRecorder!!.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH))
        }
        mediaRecorder!!.setPreviewDisplay(surfaceHolder!!.surface)
        mediaRecorder!!.setOrientationHint(mOrientation)
        if (Build.MODEL.equals("Nexus 6", ignoreCase = true) && flag == 1) {
            if (mOrientation == 90) {
                mediaRecorder!!.setOrientationHint(mOrientation)
            } else if (mOrientation == 180) {
                mediaRecorder!!.setOrientationHint(0)
            } else {
                mediaRecorder!!.setOrientationHint(180)
            }
        } else if (mOrientation == 90 && flag == 1) {
            mediaRecorder!!.setOrientationHint(270)
        } else if (flag == 1) {
            mediaRecorder!!.setOrientationHint(mOrientation)
        }
        mediaFileName = "wc_vid_" + System.currentTimeMillis()
        mediaRecorder!!.setOutputFile(folder!!.absolutePath + "/" + mediaFileName + ".mp4") // Environment.getExternalStorageDirectory()
        mediaRecorder!!.setOnInfoListener { mr, what, extra -> // TODO Auto-generated method stub
            if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED) {
                val downTime: Long = 0
                val eventTime: Long = 0
                val x = 0.0f
                val y = 0.0f
                val metaState = 0
                val motionEvent = MotionEvent.obtain(
                    downTime,
                    eventTime,
                    MotionEvent.ACTION_UP, 0f, 0f,
                    metaState
                )
                imgCapture!!.dispatchTouchEvent(motionEvent)
                Toast.makeText(
                    this@CameraActivity,
                    "You reached to Maximum(25MB) video size.",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        mediaRecorder!!.setMaxFileSize(1000 * 25 * 1000.toLong())
        try {
            mediaRecorder!!.prepare()
        } catch (e: Exception) {
            releaseMediaRecorder()
            e.printStackTrace()
            return false
        }
        return true
    }

    var myOrientationEventListener: OrientationEventListener? = null
    var iOrientation = 0
    var mOrientation = 90

    fun generateVideoThmb(srcFilePath: String?, destFile: File?) {
        try {
            val bitmap = ThumbnailUtils.createVideoThumbnail(srcFilePath, 120)
            val out = FileOutputStream(destFile)
            ThumbnailUtils.extractThumbnail(bitmap, 200, 200)
                .compress(Bitmap.CompressFormat.JPEG, 100, out)
            out.flush()
            out.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

     fun normalize(degrees: Int): Int {
        if (degrees > 315 || degrees <= 45) {
            return 0
        }
        if (degrees > 45 && degrees <= 135) {
            return 90
        }
        if (degrees > 135 && degrees <= 225) {
            return 180
        }
        if (degrees > 225 && degrees <= 315) {
            return 270
        }
        throw RuntimeException("Error....")
    }

     fun getPhotoRotation(): Int {
        val rotation: Int
        val orientation = mPhotoAngle
        val info = CameraInfo()
        if (flag == 0) {
            Camera.getCameraInfo(0, info)
        } else {
            Camera.getCameraInfo(1, info)
        }
        rotation = if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
            (info.orientation - orientation + 360) % 360
        } else {
            (info.orientation + orientation) % 360
        }
        return rotation
    }
}
