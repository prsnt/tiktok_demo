package com.example.tiktokdemo

import android.Manifest
import android.content.Intent
import android.os.Bundle
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import com.example.tiktokdemo.Camera.CameraActivity
import com.example.tiktokdemo.Camera.RunTimePermission

class MainActivity : AppCompatActivity() {

    private var runTimePermission: RunTimePermission? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        runTimePermission = RunTimePermission(this)
        runTimePermission!!.requestPermission(arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ), object : RunTimePermission.RunTimePermissionListener {
            override fun permissionGranted() {
                // First we need to check availability of play services
                startActivity(Intent(this@MainActivity, CameraActivity::class.java))
                finish()
            }

            override fun permissionDenied() {
                finish()
            }
        })
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (runTimePermission != null) {
            runTimePermission!!.onRequestPermissionsResult(
                requestCode, permissions as Array<String>,
                grantResults!!
            )
        }
    }


}